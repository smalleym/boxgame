function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}


function initializeDeck(){

  var deck = [];
  limit = 2;
  suits = ['S', 'C', 'H', 'D'];
  suitIndex = 0;
  var isRed = true;


  for(var i =0; i<52; i++){

    if(suits[suitIndex] == 'S' || suits[suitIndex] == 'C'){
      isRed = false;
    }else{
      isRed = true;
    }

    if (limit == 11){
      deck.push(['J' + suits[suitIndex], limit, isRed]);
    }else if (limit == 12){
      deck.push(['Q' + suits[suitIndex], limit, isRed]);
    }else if (limit == 13){
      deck.push(['K' + suits[suitIndex], limit, isRed]);
    }else if (limit == 14){
      deck.push(['A' + suits[suitIndex], limit, isRed]);
    }else{
      deck.push([limit + suits[suitIndex], limit, isRed]);
    }

    limit++;
    if (limit == 15){
      limit = 2;
      suitIndex++;
    }
  }

  return deck;
}

/*
 * Creates the Fade Animation when th ePlayer is Correct.
 */
function RightOrWrongAnimation(cardsLeft, verdict = 'Correct', shiftPixelsRight = 85){

  if(verdict == 'Correct'){
    glyphicon = 'glyphicon glyphicon-ok';

    if(cardsLeft == 0){
      displayMessage = "Next Player!";
      cardsUntilNextPlayer = 3;
      shiftPixelsRight = 220;
    }else{
      displayMessage = cardsLeft + " MORE"; 
      $('.red-button, .black-button').css({'display' : 'none'});
    }

  }else if (verdict == 'Wrong'){
    glyphicon = 'glyphicon glyphicon-remove';
    displayMessage = "";
  }else{
    glyphicon = 'glyphicon glyphicon-remove';
    displayMessage = "Double Time!";
  }

  $('.correct-display').html("<div class='correct-outside-display'><div class='" + glyphicon + "'><span class='correct-display-message'>" + displayMessage + "</span></div></div>");
  $('.correct-outside-display').css({'right' : shiftPixelsRight + 'px'});
  $('.correct-outside-display').fadeOut(2500);
}

function removeButtonsAndOpactity(){
  $('.card').css({'opacity': '1.0'});
  $(".higher-button").remove();
  $(".lower-button").remove();
}

function showModal(modal){
  $(modal).modal({backdrop: 'static', keyboard: false}) 
  $(modal).modal("show");
}

function countDown(countdownSecs){
  

  var x = setInterval(function() {
    // Find the distance between now and the count down date
    var distance = countdownSecs--;

    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = distance;

    $('.timer-sound')[0].play();
    $('.countdown-modal-timer').html(minutes + "m " + seconds + "s");

    showModal('#countdown-modal');

    // If the count down is finished, write some text 
    if (distance == 0) {
    $("body").append("<audio class='finished-timer-sound' src='https://actions.google.com/sounds/v1/alarms/mechanical_clock_ring.ogg'></audio>");
      //$('.finished-timer-sound')[0].autoplay = true;
      $('#countdown-modal').modal('hide');
      clearInterval(x);
      
      //$('#timer').css({'visibility': 'hidden'});
    }
  }, 1000);
}

function handleWrong(countDownSecs, wrongDouble = false, card, round = null, pixels = 85){
  param = 'Wrong';
  cardsUntilNextPlayer = 3;

  if(wrongDouble){
    param = 'Wrong Double';
    countdownSecs *= 2;  
  }

  console.log(round);
  
  RightOrWrongAnimation(cardsUntilNextPlayer, param, pixels);

  if(round == 2){
    $(card).prop('disabled', false);
  }

  $('.number-of-secs-countdown').html(countdownSecs + " s");
  showModal('#start-modal');
}


//*************** Shared Event function *****************

$(document).on('click', '.card.active', function(){
  removeButtonsAndOpactity();
  //$(this).css({'opacity': '.9'});
  //Gets the parent slot that was clicked
  parentSlot = "." + $(this).parent()[0].classList[1];
  numOfCards = $(parentSlot).find(".card").length-1;
  arrowPixelMovement = 80 + (numOfCards * pixelCounter);
  $(parentSlot + " .card").css({'opacity': '0'});
  $(this).css({'opacity': '.6'})

  //Creates Hi Low buttons
  $(parentSlot).append("<span class='button higher-button glyphicon glyphicon-arrow-up'></span>");
  $(parentSlot).append("<span class='button lower-button glyphicon glyphicon-arrow-down'></span>");
  $('.button.higher-button, .button.lower-button').css({"right" : arrowPixelMovement + "px"});
  //$('.lower-button').css({"display" : "inline-block", "position" : "relative", "top" : "290px", "right" : "-60px"});

  console.log("clicked");
});

$(document).on('click', "#deck-back", function(){
  removeButtonsAndOpactity();
});

$(document).on('click', '#start-timer-button', function(){
  countDown(countdownSecs);
});
