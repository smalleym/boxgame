//************   Global Variables ***************************

var pixelCounter = -5;
var cardsUntilNextPlayer = 4
var round = 1;
var roundMultiplier = 1;
var parentSlot = ".slot1";
var firstCardBoolean = true;
var arrowPixelMovement = 80;
var slotStartX = 580;
var wrongXPixel = -250;


//************ Functions *************************************


function round3Animation(){
	$('.card').prop('disabled', true);
	parentSlotCards = $(parentSlot).find(".card");
	parentSlotCardsSize = parentSlotCards.length;

	previousCardValue = $(parentSlot).find(".card")[parentSlotCardsSize-2].classList[1];
	lastCard = $(parentSlot).find(".card").eq(parentSlotCardsSize-1);

	lastCardValue = lastCard[0].classList[1];
	pixelsToAdd = 5 * parentSlotCardsSize;
	inButtonPixels = 695 + pixelsToAdd;
	outButton2Pixels = 330 - pixelsToAdd;
	outButton1Pixels = 1035 + pixelsToAdd;
	lastCardPixels = -320 - pixelsToAdd;

	if(parseInt(previousCardValue) < parseInt(lastCardValue)){
		inButtonPixels = 695;

		$(parentSlot).animate({'right': (pixelsToAdd += 780) + 'px'});
		$('.out-button1').css({'right': outButton1Pixels + 'px'});
		$('.in-button').css({'right' : inButtonPixels + 'px'});
		$(lastCard).animate({'right': lastCardPixels + 'px'});

	}else{
		lastCardPixels = 320;
		outButton2Pixels = 320;

		$(parentSlot).animate({'right': (pixelsToAdd += 470) + 'px'});
		$('.out-button1').css({'right': outButton1Pixels + 'px'});
		$('.in-button').css({'right' : inButtonPixels + 'px'});
		$('.out-button2').css({'right': outButton2Pixels + 'px'});
		$(lastCard).animate({'right':  lastCardPixels + 'px'});
	}

	$('.in-button, .out-button1, .out-button2').css({'display': 'inline'});

}

function endRound3Animation(){
	parentSlotCards = $(parentSlot).find(".card");
	parentSlotCardsSize = parentSlotCards.length;

	firstCard = parentSlotCards.eq(parentSlotCardsSize-1);
	secondCard = parentSlotCards.eq(parentSlotCardsSize-2);
	thirdCard = parentSlotCards.eq(parentSlotCardsSize-3);

	pixelDistance = $(thirdCard).css('right');
	pixelDistance = parseInt(pixelDistance.split("px"));

	secondCardDis = pixelDistance-5;
	firstCardDis = pixelDistance-10;

	$(secondCard).animate({'right' : secondCardDis + "px"});
	$(firstCard).animate({'right' : firstCardDis + "px", 'top' : '0px'});

	$(parentSlot).animate({"right" : slotStartX + "px", "top" : "351px"});
	$('.in-button, .out-button').css({'display' : 'none'});
}

function startRound4(){
	endRound3Animation();
	$('.card').prop('disabled', true);
	$('.suit-button').css({'display': 'inline-block'});
}

function inBetween(left, right, value, inOrOut){
	var isCorrect = false;

	if(inOrOut == 'In'){
		if(left < right){
			if(value > left && value < right){
				isCorrect = true;
			}
		}else{
			if(value < left && value > right){
				isCorrect = true;
			}
		}
	}else{

		if(left < right){
			if(value < left || value > right){
				isCorrect = true;
			}
		}else{

			if(value > left || value < right){
				isCorrect = true;
			}
		}
	}

	return isCorrect;
}


function resetRound(){
	$('.red-button, .black-button').css({'display' : 'inline'});
	$('.suit-button').css({'display' : 'none'});
	round = 1;
	cardsUntilNextPlayer = 4;
	$('.card').prop('disabled', true);
}




//**************** Event Actions **********************************

$(document).ready(function(){
	gameDeck = initializeDeck();
	shuffle(gameDeck);
	$('.slt').css({"right": "8%", "top" : "351px", 'z-index': '9'});
});


$(document).on('click', '.red-button, .black-button, .higher-button, .lower-button, .in-button, .out-button, .suit-button', function(){

	var nextCard = gameDeck.pop();
	var newChildNode;

	if(firstCardBoolean){
		imageUrl = "/images/" + nextCard[0] + ".png";
		$(parentSlot +  " .card").css("background-image", "url("+ imageUrl + ")");
		$(parentSlot +  " .card").addClass(nextCard[1].toString()).addClass(nextCard[0]).addClass(nextCard[2]).addClass("active");
		$('.slot1').animate({"right" : slotStartX + "px", "top" : "351px"});
		newChildNode = $(parentSlot + " .card");
		firstCardBoolean = false;

	}else{

		imageUrl = "/images/" + nextCard[0] + ".png";
		newChildNode = $("<div class='card " + nextCard[1] + " " + nextCard[0] +  " " + nextCard[2] + " active'></div>");
		$(newChildNode).css("background-image", "url("+ imageUrl + ")");

		numOfCards = $(parentSlot).find(".card").length;
		pixelsToMove = pixelCounter * numOfCards;
		$(newChildNode).css({'position': 'absolute', 'right': '-411px'});

		if(round == 3){
			$(newChildNode).animate({"right" : "", "top" : "-320"});
		}else{
			$(newChildNode).animate({"right" : pixelsToMove + "px", "top" : ""});
		}
	}

	previousCards = $(parentSlot).find(".card");
	previousCard = previousCards[previousCards.length - 1];
	previousCardValue = $(previousCard)[0].className.split(" ")[1];

	$(parentSlot).append(newChildNode);

	if(!gameDeck.length){
		$('#deck-back').css({'display': 'none'});
		roundComplete = true;
	}

	countdownSecs = roundMultiplier * round;

	console.log(nextCard[1]);
	console.log(previousCardValue);

	if($(this).hasClass('red-button')){
		

		//Check if card value isRed is true
		if(nextCard[2] == true){
			round++;
			RightOrWrongAnimation(--cardsUntilNextPlayer);
		}else{
			handleWrong(countdownSecs, false, newChildNode, round, wrongXPixel);
			resetRound();
		}
	}else if($(this).hasClass('black-button')){

		//Check if card value isRed is true
		if(nextCard[2] == false){
			round++;
			RightOrWrongAnimation(--cardsUntilNextPlayer);
			
		}else{
			handleWrong(countdownSecs, false, newChildNode, round, wrongXPixel);
			resetRound();
		}
	}

/*************** ROUND 2 HANDLER  **************************/

	//Actions to take when Player chooses the lower button
	else if($(this).hasClass('lower-button')){
		if(nextCard[1] < parseInt(previousCardValue)){
			round++;
			RightOrWrongAnimation(--cardsUntilNextPlayer);
			round3Animation();

		}else if (nextCard[1] > parseInt(previousCardValue)) {
			handleWrong(countdownSecs, false, newChildNode, round, wrongXPixel);
			resetRound();
		}else{
			//Case were you draw the same card, you get double the time;
			handleWrong(countdownSecs, true, newChildNode, round);
			resetRound();
		}
  

  	//Actions to Take when Player chooses Higher Button
	}else if ($(this).hasClass('higher-button')){

		if(nextCard[1] > parseInt(previousCardValue)){
			round++;
			RightOrWrongAnimation(--cardsUntilNextPlayer);
			round3Animation();

		}else if (nextCard[1] < parseInt(previousCardValue)){

			handleWrong(countdownSecs, false, newChildNode, round, wrongXPixel);
			resetRound();
		}else{
			//Case were you draw the same card, you get double the time;
			handleWrong(countdownSecs, true, newChildNode, round);
			resetRound();
		}
	}


/***************** ROUND  HANDLER ************************/

	else if($(this).hasClass('in-button')){

		previousCard2 = previousCards[previousCards.length-2];
		previousCard2Value = parseInt(previousCard2.classList[1]);

		if(inBetween(previousCard2Value, previousCardValue, nextCard[1], "In")){
			round++;
			RightOrWrongAnimation(--cardsUntilNextPlayer);
			startRound4();
		}else{
			handleWrong(countdownSecs, false, newChildNode, round, wrongXPixel);
			endRound3Animation();
			resetRound();
		}


	}else if ($(this).hasClass('out-button')){

		previousCard2 = previousCards[previousCards.length-2];
		previousCard2Value = parseInt(previousCard2.classList[1]);

		if(inBetween(previousCard2Value, previousCardValue, nextCard[1], "Out")){
			round++;
			RightOrWrongAnimation(--cardsUntilNextPlayer);
			startRound4();
		}else{
			handleWrong(countdownSecs, false, newChildNode, round, wrongXPixel);
			endRound3Animation();
			resetRound();
		}

	}

	else if($(this).hasClass('suit-button')){

		nextCardSuit = nextCard[0].split("");
		nextCardSuit = nextCardSuit[nextCardSuit.length-1];

		console.log(nextCardSuit);

		if($(this).hasClass('spade-button')){
			if(nextCardSuit == 'S'){
				RightOrWrongAnimation(--cardsUntilNextPlayer);
				resetRound();
			}else{
				handleWrong(countdownSecs, false, newChildNode, round, wrongXPixel);
				resetRound();
			}

		}else if($(this).hasClass('heart-button')){
			if(nextCardSuit == 'H'){
				RightOrWrongAnimation(--cardsUntilNextPlayer);
				resetRound();
			}else{
				handleWrong(countdownSecs, false, newChildNode, round, wrongXPixel);
				resetRound();
			}

		}else if ($(this).hasClass('club-button')){
			if(nextCardSuit == 'C'){
				RightOrWrongAnimation(--cardsUntilNextPlayer);
				resetRound();
			}else{
				handleWrong(countdownSecs, false, newChildNode, round, wrongXPixel);
				resetRound();
			}

		}else if ($(this).hasClass('diamond-button')){
			if(nextCardSuit == 'D'){
				RightOrWrongAnimation(--cardsUntilNextPlayer);
				resetRound();
			}else{
				handleWrong(countdownSecs, false, newChildNode, round, wrongXPixel);
				resetRound();
			}

		}

	}




	removeButtonsAndOpactity();
	arrowPixelMovement-=5;
});

