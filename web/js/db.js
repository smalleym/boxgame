//*********** GLOBAL Variables ****************************
var gameDeck = shuffle(initializeDeck());
var round = 1;
var initialCardCount = 9;
var wasClicked = false;
var pixelCounter = -5;
var round = 1;
var roundComplete = false;
var roundPileOffset = [10, 5, 2];
var slotsToMove = [3,2,1,4,7];
var slotsToShift = [5,6,8,9];
var cardsUntilNextPlayer = 3;
var arrowPixelMovement = 80;
var countdownSecs;



//************ Functions *************************************

/*
 *Function used for development to quickly get to end of round.
 */
function dealNumberOfCards(){

	var numOfCardsToDeal = prompt("How many Cards to Deal?");
	integerRegex = '^[0-9]+$';	

	if(!numOfCardsToDeal.match(integerRegex)){
		console.log('Value must be an integer');
	}else if(numOfCardsToDeal > gameDeck.length){
		console.log('Value exceeds number of cards left in Deck');
	}else{

		for(var i = 0; i<numOfCardsToDeal; i++){
			var min=1; 
    		var max=roundPileOffset[round-1];  
    		var randomSlotNum =Math.floor(Math.random() * (+max - +min)) + +min; 

    		if(round == 2){
    			randomSlotNum = slotsToShift[randomSlotNum-1];
    		}else if(round == 3){
    			randomSlotNum = 9;
    		}

    		var nextCard = gameDeck.pop();

    		parentSlot = '.slot' + randomSlotNum;
    		numOfCards = $(parentSlot).find(".card").length;
			pixelsToMove = pixelCounter * numOfCards;

			imageUrl = "/images/" + nextCard[0] + ".png";
			var newChildNode = $("<div class='card " + nextCard[1] + " " + nextCard[0] + " active'></div>");
			newCardOffsetY = 350 - $(parentSlot)[0].offsetTop;
			newCardOffsetX = $(parentSlot)[0].offsetLeft - 822;
	
			$(newChildNode).css("background-image", "url("+ imageUrl + ")");

			$(parentSlot).append(newChildNode);
			$(newChildNode).css({"position" : "absolute", "right" : newCardOffsetX + "px", "top" : newCardOffsetY + "px"});
			$(newChildNode).animate({"right" : pixelsToMove + "px", "top" : ""});
		}
	}
}


function nextRound(){

	if(round == 3){
		slotsToMove = slotsToShift.splice(0,3);
	}
	//animates the gathering of cards in each pile and adds the cards
	//Back into the gamedeck.
	for(var i = 0; i < slotsToMove.length; i++){
		slot = '.slot' + slotsToMove[i];
		cardsInSlot = $(slot).find(".card");
		numOfCardsInSlot = cardsInSlot.length;

		for(var j = 0; j < numOfCardsInSlot; j++){
			$(cardsInSlot).eq(j).animate({'right' : '0px'}, 1500);
			gameDeck.push([$(cardsInSlot)[j].classList[2], $(cardsInSlot)[j].classList[1]]);
		}
	}

	//Animates each of the piles going back to the deck
	for(var i = 0; i < slotsToMove.length; i++){
		slot = '.slot' + slotsToMove[i];
		$(slot).animate({"right": "8%", "top" : "351px"}, 1500);
	}

	//Puts the back of the deck back
	setTimeout(function(){
		$('#deck-back').css({'display': 'inline'});
	}, 1500);

	//Shifts the remaining piles left.
	for(var i = 0; i < slotsToShift.length; i++){
		slot = '.slot' + slotsToShift[i];
		right = $(slot).css("right");
		right = right.split("p");
		round == 3 ? right = parseInt(right[0]) - 250 : right = parseInt(right[0]) - 200;

		topAttr = $(slot).css("top");
		topAttr = topAttr.split("p");

		round == 3 ? topAttr = parseInt(topAttr[0]) + 101 : topAttr = parseInt(topAttr[0]) + 250;
		console.log(topAttr);
		console.log(right);

		$(slot).animate({"right": right + "px", "top" : topAttr + "px"}, 1500);
	}


	//Removes the slots from the field.
	(function removeSlots(i){
		setTimeout(function(){
			slot = '.slot' + slotsToMove[i];
			console.log(slot);
			$(slot).remove();
			//$(slot).css({'display' : 'none'});

			if(i--){
				removeSlots(i);
			}
		}, 1500);
	})(slotsToMove.length-1);

	//Shuffles Deck and Completes First Round
	gameDeck = shuffle(gameDeck);
	roundComplete = false;
}

//***********************************************************************

$(document).on('click', '.deal-number-of-cards', function(){
	dealNumberOfCards();
});

$(document).on("click", ".higher-button, .lower-button", function(){
	console.log("clicked");

	//Gets the next Card out of the Global Deck variable
	var nextCard = gameDeck.pop();

	//Gets the parent slot that was clicked
	parentSlot = "." + $(this).parent()[0].classList[1];

	//Finds the previous Card and splits its class value with also
	//stores the value of the card.
	previousCard = $(parentSlot).find(".card");
	previousCard = previousCard[previousCard.length - 1];
	previousCardValue = $(previousCard)[0].className.split(" ")[1];

	$(previousCard).removeClass("active").addClass("not-active");
	$(previousCard).prop("disabled", true);

	//Gets the pixel difference of the current Pile
	numOfCards = $(parentSlot).find(".card").length;
	pixelsToMove = pixelCounter * numOfCards;

	//Gets image of the next Card plucked from the deck and appends
	//a new card div to slot
	imageUrl = "/images/" + nextCard[0] + ".png";
	var newChildNode = $("<div class='card " + nextCard[1] + " " + nextCard[0] + " active'></div>");
	newCardOffsetY = 350 - $(parentSlot)[0].offsetTop;
	newCardOffsetX = $(parentSlot)[0].offsetLeft - 822;
	
	$(newChildNode).css("background-image", "url("+ imageUrl + ")");
	//$(newChildNode).css({"right": "100px", "top": "350"});


	$(parentSlot).append(newChildNode);
	$(newChildNode).css({"position" : "absolute", "right" : newCardOffsetX + "px", "top" : newCardOffsetY + "px"});
	$(newChildNode).animate({"right" : pixelsToMove + "px", "top" : ""});

	if(!gameDeck.length){
		$('#deck-back').css({'display': 'none'});
		roundComplete = true;
		round++;
	}

	countdownSecs = $(parentSlot).find(".card").length;

	//Actions to take when Player chooses the lower button
	if($(this).hasClass('lower-button')){
		if(nextCard[1] < parseInt(previousCardValue)){
			RightOrWrongAnimation(--cardsUntilNextPlayer);

		}else if (nextCard[1] > parseInt(previousCardValue)) {
			handleWrong(countdownSecs);
		}else{
			//Case were you draw the same card, you get double the time;
			handleWrong(countdownSecs, true);
		}
  

  	//Actions to Take when Player chooses Higher Button
	}else if ($(this).hasClass('higher-button')){

		if(nextCard[1] > parseInt(previousCardValue)){
			RightOrWrongAnimation(--cardsUntilNextPlayer);

		}else if (nextCard[1] < parseInt(previousCardValue)){

			handleWrong(countdownSecs);
		}else{
			//Case were you draw the same card, you get double the time;
			handleWrong(countdownSecs, true);
		}
	}

	removeButtonsAndOpactity();
	arrowPixelMovement-=5;

	if(round == 2 && roundComplete){
		nextRound();
	}else if(round == 3 && roundComplete){
		nextRound();
	}
});


$(document).on('click', '.start-deathbox-game', function(){
	var players = [];
	$('option').each(function(){
		players.push(parseInt($(this).prop('id')));
	});

	$('.field-play').show();
	$('.choose-players-div').hide();
	$('.play-button').hide();

	$.ajax({  
       url:        '/new-game',
       type:       'POST',  
       dataType:   'html',
       async:      true,  
       data: {"players" : players},

       success: function(data, status) {  
           console.log(data);
           players = data;
       },  

       error: function(xhr, textStatus, errorThrown) {  
           alert("An error occurred: " + errorThrown);
       }  
    });


	var imageUrl = "";
	var slot = 0;
	var nextCard;
	var fieldArray = [];
	var xIndex = 1000;
	var yIndex = 0;
	var row = 1;

	var firstPlayer = Math.floor(Math.random() * players.length);

	$('.timer-sound')[0].playbackRate = 2.0;
	$('.slt').css({"right": "8%", "top" : "351px", 'z-index': '9'});
	setTimeout((function dealLoop(i) {

		setTimeout(function(){
			nextCard = gameDeck.pop();
			slot = ".slot" + i;

			imageUrl = "/images/" + nextCard[0] + ".png";
			$(slot +  " .card").css("background-image", "url("+ imageUrl + ")");
			$(slot +  " .card").addClass(nextCard[1].toString()).addClass(nextCard[0]).addClass("active");

			if(row < 4){
				$(slot).css({"right": "8%", "top" : "351px"});
				$(slot).animate({"right": xIndex + "px", "top" : yIndex + "px"}, 1000);
				xIndex -= 300;
				row++;
			}else{
				xIndex=1000;
				yIndex += 350;
				$(slot).css({"right": "8%", "top" : "351px"});
				$(slot).animate({"right": xIndex + "px", "top" : yIndex + "px"}, 1000);
				xIndex -= 300;
				row=2;
			}

			if(--i){
				dealLoop(i);
			}
		}, 50);
	})(9)
	, 3000);

	$('.deal-number-of-cards').css({'display' : 'inline'})
});

