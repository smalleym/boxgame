<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Game;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/home.html.twig');
    }


    /**
     *  @Route("/play/db", name="play-deathbox")
     */
    public function playDBAction(Request $request){
        $em = $this->getDoctrine()->getManager();

        $players = $em->getRepository('AppBundle:Player')->findAll();

        return $this->render('default/index.html.twig', array(
            'players' => $players,
        ));
    }


    /**
     *  @Route("/play/rtw", name="play-ride-the-wave")
     */
    public function gameRTWAction(Request $request){
        return $this->render('default/rideTheWave.html.twig');
    }

    /**
     *  @Route("/rules", name="game_rules")
     */
    public function rulesAction(){
        return $this->render('default/rules.html.twig');
    }

    /**
     *  @Route("/new-game", name="create-new-game")
     */
    public function createNewGame(Request $request){
        $em = $this->getDoctrine()->getManager();
        $playersInGameIds = $request->request->get('players');
        $playerEntities = [];
        $newGame = new Game();

        if($playersInGameIds && count($playersInGameIds)){
            foreach($playersInGameIds as $id){
                $player = $em->getRepository('AppBundle:Player')->find($id);
                $playerEntities['name'] = $player->getName();
                $playerEntities['id'] = $player->getId();
                $newGame->addPlayer($player);
            }
        }
        
        $response =  new JsonResponse();
        $response->setData(array(
            'players' => $playerEntities,
        ));

        return $response;
    }
}
