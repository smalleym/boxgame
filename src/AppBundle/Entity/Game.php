<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Game
 *
 * @ORM\Table(name="game")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GameRepository")
 */
class Game
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="round", type="integer")
     */
    private $round;

    /**
     * @var int
     *
     * @ORM\Column(name="totalSeconds", type="integer")
     */
    private $totalSeconds;

    /**
     * @var bool
     *
     * @ORM\Column(name="current_game", type="boolean", nullable=true)
     */
    private $currentGame;

    /**
     * @var bool
     *
     * @ORM\Column(name="game_finished", type="boolean", nullable=true)
     */
    private $gameFinished;

    /**
     * @var string|null
     *
     * @ORM\Column(name="last_drinker", type="string", length=255, nullable=true)
     */
    private $lastDrinker;

    /**
     * @ORM\ManyToMany(targetEntity="Player", inversedBy="gamesPlayed")
     **/
    private $players;

    public function __toString(){
        return (string) $this->id; 
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set round.
     *
     * @param int $round
     *
     * @return Game
     */
    public function setRound($round)
    {
        $this->round = $round;

        return $this;
    }

    /**
     * Get round.
     *
     * @return int
     */
    public function getRound()
    {
        return $this->round;
    }

    /**
     * Set currentGame.
     *
     * @param bool $currentGame
     *
     * @return Game
     */
    public function setCurrentGame($currentGame)
    {
        $this->currentGame = $currentGame;

        return $this;
    }

    /**
     * Get currentGame.
     *
     * @return bool
     */
    public function getCurrentGame()
    {
        return $this->currentGame;
    }

    /**
     * Set gameFinished.
     *
     * @param bool $gameFinished
     *
     * @return Game
     */
    public function setGameFinished($gameFinished)
    {
        $this->gameFinished = $gameFinished;

        return $this;
    }

    /**
     * Get gameFinished.
     *
     * @return bool
     */
    public function getGameFinished()
    {
        return $this->gameFinished;
    }

    /**
     * Set lastDrinker.
     *
     * @param string|null $lastDrinker
     *
     * @return Game
     */
    public function setLastDrinker($lastDrinker = null)
    {
        $this->lastDrinker = $lastDrinker;

        return $this;
    }

    /**
     * Get lastDrinker.
     *
     * @return string|null
     */
    public function getLastDrinker()
    {
        return $this->lastDrinker;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->players = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set totalSeconds.
     *
     * @param int $totalSeconds
     *
     * @return Game
     */
    public function setTotalSeconds($totalSeconds)
    {
        $this->totalSeconds = $totalSeconds;

        return $this;
    }

    /**
     * Get totalSeconds.
     *
     * @return int
     */
    public function getTotalSeconds()
    {
        return $this->totalSeconds;
    }

    /**
     * Add player.
     *
     * @param \AppBundle\Entity\Player $player
     *
     * @return Game
     */
    public function addPlayer(\AppBundle\Entity\Player $player)
    {
        $this->players[] = $player;

        return $this;
    }

    /**
     * Remove player.
     *
     * @param \AppBundle\Entity\Player $player
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePlayer(\AppBundle\Entity\Player $player)
    {
        return $this->players->removeElement($player);
    }

    /**
     * Get players.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayers()
    {
        return $this->players;
    }
}
