<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Player
 *
 * @ORM\Table(name="player")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PlayerRepository")
 */
class Player
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(name="turn", type="boolean", nullable=true)
     */
    private $turn;

    /**
     * @var int|null
     *
     * @ORM\Column(name="rounds_played", type="integer", nullable=true)
     */
    private $roundsPlayed;

    /**
     * @ORM\ManyToMany(targetEntity="Game", mappedBy="players")
     */   
    private $gamesPlayed;

    /**
     * @var int|null
     *
     * @ORM\Column(name="total_seconds_drinking", type="integer", nullable=true)
     */
    private $totalSecondsDrinking;

    /**
     * @var int|null
     *
     * @ORM\Column(name="games_finished", type="integer", nullable=true)
     */
    private $gamesFinished;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Player
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set turn.
     *
     * @param bool $turn
     *
     * @return Player
     */
    public function setTurn($turn)
    {
        $this->turn = $turn;

        return $this;
    }

    /**
     * Get turn.
     *
     * @return bool
     */
    public function getTurn()
    {
        return $this->turn;
    }

    /**
     * Set roundsPlayed.
     *
     * @param int|null $roundsPlayed
     *
     * @return Player
     */
    public function setRoundsPlayed($roundsPlayed = null)
    {
        $this->roundsPlayed = $roundsPlayed;

        return $this;
    }

    /**
     * Get roundsPlayed.
     *
     * @return int|null
     */
    public function getRoundsPlayed()
    {
        return $this->roundsPlayed;
    }

    /**
     * Set gamesPlayed.
     *
     * @param int|null $gamesPlayed
     *
     * @return Player
     */
    public function setGamesPlayed($gamesPlayed = null)
    {
        $this->gamesPlayed = $gamesPlayed;

        return $this;
    }

    /**
     * Get gamesPlayed.
     *
     * @return int|null
     */
    public function getGamesPlayed()
    {
        return $this->gamesPlayed;
    }

    /**
     * Set totalSecondsDrinking.
     *
     * @param int|null $totalSecondsDrinking
     *
     * @return Player
     */
    public function setTotalSecondsDrinking($totalSecondsDrinking = null)
    {
        $this->totalSecondsDrinking = $totalSecondsDrinking;

        return $this;
    }

    /**
     * Get totalSecondsDrinking.
     *
     * @return int|null
     */
    public function getTotalSecondsDrinking()
    {
        return $this->totalSecondsDrinking;
    }

    /**
     * Set gamesFinished.
     *
     * @param int|null $gamesFinished
     *
     * @return Player
     */
    public function setGamesFinished($gamesFinished = null)
    {
        $this->gamesFinished = $gamesFinished;

        return $this;
    }

    /**
     * Get gamesFinished.
     *
     * @return int|null
     */
    public function getGamesFinished()
    {
        return $this->gamesFinished;
    }
}
