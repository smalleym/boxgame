BoxGame
=======

A Symfony project created on July 2, 2019, 3:48 pm.

This is a project (unfinished) that consists of two card games thus far that I intended on putting on a server for others to play. 
It is a Symfony project, where most of the work is front end work so mainly Javascript with minimum backend PHP written in the controllers. 

Files to look at:

In boxgame/web/js:

script.js 			//The main script file that is linked into the document before the the two games below.
db.js
rideTheWave.js

In boxgame/src/AppBundle/Controller

DefaultController.php   
PlayerController.php

